FROM gluster/gluster-centos:gluster4u0_centos7

COPY ./gluster-exporter.service /usr/lib/systemd/system/
COPY ./gluster-exporter.toml /etc/gluster-exporter/gluster-exporter.toml

ADD https://github.com/maksim-paskal/gluster-prometheus/releases/download/1.1/gluster-exporter /usr/local/sbin/gluster-exporter

RUN chmod +x /usr/local/sbin/gluster-exporter \
    && systemctl enable gluster-exporter
